const router = require('koa-router')()
import jwt from '../middlewares/jwt'
import { find } from '../controllers/questions'

router.prefix('/questions')

router.get('/questions', find)
// .post('/question', add)
// .del('/:id', del)
// .put('/:id', put)

export default router
