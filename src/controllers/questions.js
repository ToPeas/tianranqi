import _ from 'lodash'
const { Question, Primary } = require('../models/questions')

export const find = async (ctx, next) => {
  const { type } = ctx.query
  // console.log(Object.prototype.toString.call(refresh))
  // console.log(ctx.session.questions.length)
  // if (refresh === 'true') {
  // console.log('进来了')
  if (type === 'primary') {
    const data = await Primary.aggregate([{ $sample: { size: 200 } }])
    ctx.session = {
      questions: data
    }
    return ctx.success('获取成功', data, 200)
  }
  // const data = await Primary.aggregate([{ $sample: { size: 4 } }])
  ctx.success('获取成功', [], 200)
  // } else {
  //   // console.log('缓存')
  //   return ctx.success('获取缓存成功', ctx.session.questions, 200)
  // }
  // if (ctx.session.questions && refresh) {
  //   console.log('缓存')

  // } else {
  //   const data = await Primary.aggregate([{ $sample: { size: 100 } }])
  //   ctx.session = {
  //     questions: data
  //   }
  //   ctx.success('获取成功', data, 200)
  // }
  // const data = await Primary.aggregate([{ $sample: { size: 100 } }])
  // const _data = _.uniqBy(data, 'id')
  // const data = await Primary.find()
  // console.log(data.length)
  // console.log(_data.length)
  next()
}
